##########################################
#
#作成した姿勢モデルの確認
#
#########################################
import sys
import tkinter 
import pandas as pd
import csv
from time import sleep
from PIL import Image, ImageTk
import threading
from datetime import datetime
import socket
import struct
import pickle
import numpy as np
def csv_reader(key_file,skel_file):
    keypoint_csv = pd.read_csv(key_file,header=None)
    keypoint = keypoint_csv.values.tolist()
    keypoint_model = pd.DataFrame(data = keypoint, columns = ["part","score","x","y"])
    skeleton_csv = pd.read_csv(skel_file,header=None)
    skeleton = skeleton_csv.values.tolist()
    skeleton_model = pd.DataFrame(data = skeleton, columns = ["partA.x","partA.y","partB.x","partB.y"])

    #DataFrameをlist型に変換
    keypoint_model = keypoint_model.values.tolist()
    skeleton_model = skeleton_model.values.tolist()

    return keypoint_model,skeleton_model



#
# GUI設定
#
root = tkinter.Tk()
root.title(u"TkinterのCanvasを使ってみる")
root.geometry("800x400")

#キャンバスエリア
canvas = tkinter.Canvas(root, width = 800, height = 400)#Canvasの作成
#canvas.pack()
#キャンバスバインド
canvas.place(x=0, y=0)#Canvasの配置
count = 0
"""
model = ["0","1","2","3","4","5","6","7","8","9",
        "10","11","10","9","8","7","6","5","4","3","2","1"]
"""
model = ["0","1","2","3","4","5","6","7","8","9",
        "10","11","12","13","14","15","16","17","18","19","20","21"]
sensor = []
global clf

#with open('sisei_squ.pickle', mode='rb') as fp:
#    clf = pickle.load(fp)

def update():
    global count
    canvas.create_rectangle(0, 0, 800, 400, fill = 'white')#塗りつぶし
    key_file='data/keypoints/key_walk/keypoint_walk{}.csv'.format(count)
    skel_file='data/skeletons/skel_walk/skeleton_walk{}.csv'.format(count)
    keypoint,skelton = csv_reader(key_file,skel_file)
    print(canvas)
    print(count)
    for key in keypoint:
        canvas.create_oval(key[2]+5, key[3]+5, key[2]-5, key[3]-5, tag="oval0", fill = 'Blue')

    for skel in skelton:
        canvas.create_line(skel[0], skel[1], skel[2], skel[3], fill='black')
    count += 1 
    if count > 21:
        count = 0
        canvas.delete("all")


    root.after(10000,update)
update()
#
# GUIの末端
#
root.mainloop()